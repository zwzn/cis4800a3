package object

import (
	"math"
)

func NewSphere(sections int) *Mesh {
	var m *Mesh
	m = newMesh()
	dps := math.Pi * 2 / float64(sections)
	for pitch := 0.0; pitch < math.Pi+dps*1.5; pitch += dps {
		for yaw := 0.0; yaw < math.Pi*2; yaw += dps {
			var p1, p2, p3, p4 *Vector3
			p1 = NewDirectionVector3(yaw, pitch)
			p2 = NewDirectionVector3(yaw, pitch+dps)
			p3 = NewDirectionVector3(yaw+dps, pitch)
			p4 = NewDirectionVector3(yaw+dps, pitch+dps)

			m.addTriangle(NewTriangle(p1, p2, p3, p1, p2, p3))
			m.addTriangle(NewTriangle(p2, p3, p4, p2, p3, p4))
		}
	}
	return m
}

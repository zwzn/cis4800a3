package object

import "fmt"

type Basis struct {
	I *Vector3
	J *Vector3
	K *Vector3
}

var IdentityBasis = NewBasis(
	1, 0, 0,
	0, 1, 0,
	0, 0, 1)

// NewBasis creates a new basis object from 9 float64 values
func NewBasis(
	ix, iy, iz,
	jx, jy, jz,
	kx, ky, kz float64) *Basis {

	return &Basis{
		Vec3(ix, iy, iz),
		Vec3(jx, jy, jz),
		Vec3(kx, ky, kz),
	}
}

// NewBasis creates a new basis object from 9 float64 values
func (b *Basis) Clone() *Basis {
	return &Basis{
		b.I.Clone(),
		b.J.Clone(),
		b.K.Clone(),
	}
}

func (b *Basis) String() string {
	return fmt.Sprintf("|%.2f %.2f %.2f|\n|%.2f %.2f %.2f|\n|%.2f %.2f %.2f|",
		b.I.X, b.J.X, b.K.X,
		b.I.Y, b.J.Y, b.K.Y,
		b.I.Z, b.J.Z, b.K.Z)
}

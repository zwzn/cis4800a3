package main

import (
	"flag"
	"image"
	"image/png"
	"log"
	"os"

	"bitbucket.org/zwzn/cis4800a3/object"
	"bitbucket.org/zwzn/cis4800a3/renderer"
	"github.com/nfnt/resize"
)

func check(e error) {
	if e != nil {
		log.Fatalf("%v\n", e)
	}
}

func main() {

	inFile := flag.String("if", "", "an xobj file to be rendered")
	outFile := flag.String("of", "image.png", "the image file to output")
	rend := flag.String("renderer", "a2", "The renderer to use")
	size := flag.Int("size", 1000, "The size of the output image")
	camera := flag.String("camera", "", "The name of the camera to use of empty for default")
	c := flag.String("c", "", "An alias for camera")
	aa := flag.Int("aa", 1, "Anti Aliasing")
	flag.Parse()

	if *c != "" {
		camera = c
	}

	if *inFile == "" {
		log.Fatalf("You must enter an xobj file to be rendered\n")
	}

	obj, err := object.FromFile(*inFile)
	check(err)
	var img image.Image
	if *rend == "a2" {
		img, err = renderer.RenderObject(obj, (*size)*(*aa), (*size)*(*aa), *camera)
		check(err)
	} else {
		img, err = renderer.RenderObject(obj, (*size)*(*aa), (*size)*(*aa), *camera)
		check(err)
	}
	if *aa != 1 {
		img = resize.Resize(uint(*size), uint(*size), img, resize.Lanczos3)
	}
	saveImage(img, *outFile)
}

func saveImage(img image.Image, file string) {

	f, err := os.Create(file)
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(f, img); err != nil {
		f.Close()
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}

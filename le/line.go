package le

import "fmt"

type Line struct {
	Point     *Vector3
	Direction *Vector3
}

func NewLineFromPoints(p1, p2 *Vector3) *Line {
	return &Line{p1, p2.Subtract(p1)}
}

func (l *Line) GetPoint(d float64) *Vector3 {
	return l.Point.Add(l.Direction.Scalar(d))
}

func (l *Line) String() string {
	return fmt.Sprintf("Point: %v\nDirection: %v", l.Point, l.Direction)
}
